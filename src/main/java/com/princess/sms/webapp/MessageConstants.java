package com.princess.sms.webapp;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
public enum MessageConstants {

  NO_PRINTER_SET(
      "no.printer.set");

  private String key;

  /**
   * Constructor for ExceptionCode.
   * 
   * @param key String
   */
  private MessageConstants(String key) {
    this.key = key;
  }

  /**
   * Method toString.
   * 
   * 
   * 
   * 
   * 
   * @return String
   */
  @Override
  public String toString() {
    return key;
  }

}
