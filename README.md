# SMS (Princess JAVA Developer Candidate Exam) #

This application is to test a candidate's development abilities. Fork this project, make your changes, and create pull request. https://confluence.atlassian.com/bitbucket/fork-a-repo-compare-code-and-create-a-pull-request-269981804.html#notfound

### PRE-REQUISITE ###

* JAVA
* Gradle
* IDE
* Findbugs

### WS END-POINTS USED ###

* http://fitpexam.getsandbox.com/sms/service/languages/supported
* http://fitpexam.getsandbox.com/sms/service/voyages/current

### Tasks ###

* Unit Test for Language.java
* Unit Test for LanguageServiceImpl.java
* Integration Test for LanguageServiceImpl.java
* Integration Test for LanguageController.java
* Figure out why the unit test LanguageControllerTest.java is failing
* Make PrintUtil.java cannot be instantiated and extended
* Fix all Findbugs issues